Name:           glibmm24
Version:        2.64.5
Release:        1
Summary:        C++ interface for the GLib library
License:        LGPLv2+
URL:            http://www.gtkmm.org/
Source0:        http://ftp.gnome.org/pub/GNOME/sources/glibmm/2.64/glibmm-%{version}.tar.xz

BuildRequires:  gcc-c++ glib2-devel >= 2.61.2  libsigc++20-devel >= 2.9.1
BuildRequires:  m4 perl-generators perl-interpreter perl-Getopt-Long
BuildRequires:  meson doxygen libxslt mm-common graphviz

Requires:       glib2 >= 2.61.2 libsigc++20 >= 2.9.1

%description
The glibmm package is a set of C++ bindings for Glib.
It provides non-UI API that is not available in standard C++
and makes it possible for gtkmm to wrap GObject-based APIs.

%package devel
Summary:        Headers for developing programs using glibmm24
Requires:       %{name} = %{version}-%{release}

%description devel
The glibmm24-devel package contains the static libraries and
header files needed for developing glibmm applications.

%package        help
Provides:       %{name}-doc = %{version}-%{release}
Obsoletes:      %{name}-doc < %{version}-%{release}
Summary:        full API documentation for glibmm24
BuildArch:      noarch
Requires:       %{name} = %{version}-%{release}
Requires:       libsigc++20-doc

%description    help
The glibmm24-help package contains the full API documentation for glibmm24.


%prep
%autosetup -n glibmm-%{version}


%build
%meson -Dbuild-documentation=true
%meson_build

%install
%meson_install
%delete_la


%files
%doc AUTHORS NEWS README COPYING
%{_libdir}/*.so.*

%files devel
%{_includedir}/{glibmm-2.4,giomm-2.4}/
%{_libdir}/*.so
%{_libdir}/{glibmm-2.4,giomm-2.4}/
%{_libdir}/pkgconfig/*.pc

%files help
%doc %{_datadir}/devhelp/
%doc %{_docdir}/glibmm-2.4/


%changelog
* Fri Jun 18 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 2.64.5-1
- Upgrade to 2.64.5
- Use meson rebuild

* Thu Dec 12 2019 zoushuangshuang<zoushuangshuang@huawei.com> - 2.58.0-2
- Package init
